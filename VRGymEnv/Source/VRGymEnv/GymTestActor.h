// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "VRGymEnv.h"
#include "GameFramework/Actor.h"

#include "ImageUtils.h"

#include "GymTestActor.generated.h"

UCLASS()
class VRGYMENV_API AGymTestActor : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AGymTestActor();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

private:
	FString CommandLine;
	FViewport* InViewport;

	// communication testing
	int PortEnv;
	int PortRecv;
};