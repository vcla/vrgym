// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "VRGymEnv.h"
#include "GameFramework/MovementComponent.h"
#include "AgentMovementComponent.generated.h"

/**
 * 
 */
UCLASS()
class VRGYMENV_API UAgentMovementComponent : public UMovementComponent
{
	GENERATED_BODY()
	
public:
	virtual void TickComponent(float DeltaTime, enum ELevelTick TickType, FActorComponentTickFunction *ThisTickFunction) override;
	bool SafeMoveComponent(FVector Motion, float MovementSpeed, float DeltaTime);
	
};
