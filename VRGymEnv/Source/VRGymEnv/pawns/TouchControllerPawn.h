// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "VRGymEnv.h"
#include "BasePawn.h"
#include "NormalPawnMovementComponent.h"
#include "GameFramework/Pawn.h"
#include "TouchControllerPawn.generated.h"

UCLASS()
class VRGYMENV_API ATouchControllerPawn : public ABasePawn
{
	GENERATED_BODY()

public:
	UPROPERTY(EditAnywhere, BluePrintReadWrite, Category = "Motion Controller Pawn")
		FVector LeftHandLocation;

	UPROPERTY(EditAnywhere, BluePrintReadWrite, Category = "Motion Controller Pawn")
		FVector RightHandLocation;

	UPROPERTY(EditAnywhere, BluePrintReadWrite, Category = "Motion Controller Pawn")
		FRotator LeftHandRotation;

	UPROPERTY(EditAnywhere, BluePrintReadWrite, Category = "Motion Controller Pawn")
		FRotator RightHandRotation;

	UFUNCTION(BlueprintCallable, Category = "Motion Controller Pawn")
		FVector ReturnLeftHandLocation();

	UFUNCTION(BlueprintCallable, Category = "Motion Controller Pawn")
		FVector ReturnRightHandLocation();

	UFUNCTION(BlueprintCallable, Category = "Motion Controller Pawn")
		FRotator ReturnLeftHandRotation();

	UFUNCTION(BlueprintCallable, Category = "Motion Controller Pawn")
		FRotator ReturnRightHandRotation();

	// Grabing
	UFUNCTION(BlueprintCallable, Category = "Motion Controller Pawn")
		void LeftHandGrabbingIsTrue();

	UFUNCTION(BlueprintCallable, Category = "Motion Controller Pawn")
		void LeftHandGrabbingIsFalse();

	UFUNCTION(BlueprintCallable, Category = "Motion Controller Pawn")
		void RightHandGrabbingIsTrue();

	UFUNCTION(BlueprintCallable, Category = "Motion Controller Pawn")
		void RightHandGrabbingIsFalse();

	// Pointing
	UFUNCTION(BlueprintCallable, Category = "Motion Controller Pawn")
		void LeftHandPointingIsTrue();

	UFUNCTION(BlueprintCallable, Category = "Motion Controller Pawn")
		void LeftHandPointingIsFalse();

	UFUNCTION(BlueprintCallable, Category = "Motion Controller Pawn")
		void RightHandPointingIsTrue();

	UFUNCTION(BlueprintCallable, Category = "Motion Controller Pawn")
		void RightHandPointingIsFalse();

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Motion Controller Pawn")
		USceneComponent* SceneComponent;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Motion Controller Pawn")
		USphereComponent* SphereComponent;

	UPROPERTY(EditDefaultsOnly, Category = "Motion Controller Pawn")
		USkeletalMeshComponent* BodyMesh;

	// Sets default values for this pawn's properties
	ATouchControllerPawn();

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

private:
	virtual UPawnMovementComponent* GetMovementComponent() const override;
	
	bool bIsLeftHandGrabbing;
	bool bIsRightHandGrabbing;
	bool bIsLeftHandPointing;
	bool bIsRightHandPointing;

	void PawnForward(float AxisValue);
	void PawnRight(float AxisValue);
	void PawnTurn(float AxisValue);
};
