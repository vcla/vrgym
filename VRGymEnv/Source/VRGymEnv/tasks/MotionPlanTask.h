// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "VRGymEnv.h"
#include "UObject/NoExportTypes.h"

#include "BaseAgent.h"
#include "MessageParser.h"

#include "MotionPlanTask.generated.h"

/**
 * 
 */
UCLASS()
class VRGYMENV_API UMotionPlanTask : public UObject, public IMessageParser
{
	GENERATED_BODY()
	
public:
	UMotionPlanTask();

	void MotionPlanTransform(ABaseAgent* MotionPlanAgent, TMap<FString, UStaticMeshComponent*> &NameToMeshMap, TArray<TTuple<FString, FTransform> > &AgentJointsTransforms);

	virtual bool MotionPlanMsgParser(std::string Message, FString AgentName, TArray<TTuple<FString, FTransform> > &AgentJointsTransforms) override;
	
};
