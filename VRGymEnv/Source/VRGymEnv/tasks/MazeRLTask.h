// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "VRGymEnv.h"
#include "UObject/NoExportTypes.h"

#include "ImageUtils.h"
#include "BaseAgent.h"
#include "MessageParser.h"
#include "MessageWrapper.h"

#include "MazeRLTask.generated.h"

/**
 * 
 */
UCLASS()
class VRGYMENV_API UMazeRLTask : public UObject, public IMessageParser, public IMessageWrapper
{
	GENERATED_BODY()

public:
	UMazeRLTask();

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Task Property")
		FVector StartLoc;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Task Property")
		FVector EndLoc;

	// Message handler
	virtual bool RLMazeResetMsgParser(std::string Message, FString AgentName, uint64* TimeStamp, bool* Reply) override;
	virtual bool RLMazeStepMsgParser(std::string Message, FString AgentName, int ActionDim, float* AgentAction, uint64* TimeStamp, bool* Reply) override;

	virtual std::string RLMazeResetMsgWrapper(FString AgentName, std::string State, uint64 TimeStamp) override;
	virtual std::string RLMazeStepMsgWrapper(FString AgentName, std::string NextState, float Reward, bool Terminal, uint64 TimeStamp) override;

	// Task execution
	void MazeTaskInit(ABaseAgent* RLAgent);
	void MazeReset(ABaseAgent* RLAgent);
	void MazeStep(ABaseAgent* RLAgent, int ActionDim, float* AgentAction);
	float MazeRewardFun(ABaseAgent* RLAgent);
	bool MazeTerminal(ABaseAgent* RLAgent);

	// Task properties
	uint8 GetActionDim() const;
	uint8 GetFrameSize() const;
	void SetRenderTarget(UTextureRenderTarget2D* AgentRenderTarget);

private:
	const uint32 _MaxSteps = 500;
	const float _ReachDistance = 100.0;
	const float _UnitMovementX = 50.0;
	const float _UnitMovementY = 50.0;
	const float _UnitMovementYaw = 90.0;
	const uint8 _FrameSize = 84;
	const uint8 _ActionDim = 3;

	bool _bSafeMovement;
	uint32 _CurrSteps;
	uint64 _TaskTimeStamp;
	std::string _TaskLastMsg;

	void SceneCapture(TArray<FColor> &State);
	UTextureRenderTarget2D* _TaskRenderTarget2D;
};
