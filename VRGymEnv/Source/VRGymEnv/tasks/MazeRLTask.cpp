// Fill out your copyright notice in the Description page of Project Settings.

#include "MazeRLTask.h"
#include <google/protobuf/text_format.h>


static void ColorToRGB(TArray<FColor> &State, TArray<uint8> &RGBState)
{
	RGBState.Empty();
	for (auto &Color : State)
	{
		uint8 R = Color.R;
		uint8 G = Color.G;
		uint8 B = Color.B;

		RGBState.Emplace(R);
		RGBState.Emplace(G);
		RGBState.Emplace(B);
	}
}

void UMazeRLTask::SceneCapture(TArray<FColor> &State)
{
	int32 Width = _TaskRenderTarget2D->SizeX; int32 Height = _TaskRenderTarget2D->SizeY;
	State.AddZeroed(Width*Height);
	FTextureRenderTargetResource* RenderTargetResource;
	RenderTargetResource = _TaskRenderTarget2D->GameThread_GetRenderTargetResource();
	FReadSurfaceDataFlags ReadSurfaceDataFlags;
	ReadSurfaceDataFlags.SetLinearToGamma(false);

	RenderTargetResource->ReadPixels(State, ReadSurfaceDataFlags);
}

UMazeRLTask::UMazeRLTask()
{
	StartLoc = FVector(-2075.458984, 137.255005, 154.848007);
	EndLoc = FVector(-1324.656006, -88.345001, 154.848007);

	_bSafeMovement = true;
	_CurrSteps = 0;
}

void UMazeRLTask::MazeTaskInit(ABaseAgent* RLAgent)
{
	APlayerController* PlayerController = UGameplayStatics::GetPlayerController(GetWorld(), 0);
	if (PlayerController) PlayerController->SetViewTarget(RLAgent);

	_TaskTimeStamp = 0;
}

void UMazeRLTask::MazeReset(ABaseAgent* RLAgent)
{
	RLAgent->SetActorLocation(StartLoc);
	RLAgent->SetActorRotation(FRotator(0.0, -90.0, 0.0));

	std::string TaskMsg;
	if (_TaskTimeStamp != RLAgent->GetTimeStamp())
	{
		TArray<FColor> State;
		SceneCapture(State);

		TArray<uint8> RGBState;
		ColorToRGB(State, RGBState);
		uint8* BytesState = RGBState.GetData();

		std::string StrState(reinterpret_cast<const char*>(BytesState), 3*_FrameSize*_FrameSize);

		TaskMsg = RLMazeResetMsgWrapper(RLAgent->GetName(), StrState, RLAgent->GetTimeStamp());
		_CurrSteps = 0;
	}
	else TaskMsg = _TaskLastMsg;

	RLAgent->SetTaskMsg(TaskMsg);
	_TaskTimeStamp = RLAgent->GetTimeStamp();
	_TaskLastMsg = TaskMsg;
}

void UMazeRLTask::MazeStep(ABaseAgent* RLAgent, int ActionDim, float* AgentAction)
{
	FVector AgentLoc = RLAgent->GetActorLocation();
	FRotator AgentRot = RLAgent->GetActorRotation();

	std::string TaskMsg;
	if (_TaskTimeStamp != RLAgent->GetTimeStamp())
	{
		float ActionXDim = AgentAction[0] * _UnitMovementX;
		float ActionYDim = AgentAction[1] * _UnitMovementY;
		float ActionYaw = AgentAction[2] * _UnitMovementYaw;

		// State transition
		AgentRot.Yaw += ActionYaw;
		RLAgent->SetActorRotation(AgentRot);
		FVector DisplacementVector = FVector(0.0, 0.0, 0.0);
		DisplacementVector = RLAgent->GetActorForwardVector()*ActionXDim + RLAgent->GetActorRightVector()*ActionYDim;
		_bSafeMovement = RLAgent->_MovementComponent->SafeMoveComponent(DisplacementVector, 0.0f, 0.0f);

		GEngine->AddOnScreenDebugMessage(-1, 5.0, FColor::Red, FString::Printf(TEXT("%s"), *RLAgent->GetActorLocation().ToString()));

		// State returns
		TArray<FColor> NextState;
		SceneCapture(NextState);

		TArray<uint8> RGBNextState;
		ColorToRGB(NextState, RGBNextState);

		uint8* BytesNextState = RGBNextState.GetData();
		std::string StrNextState(reinterpret_cast<const char*>(BytesNextState), 3*_FrameSize*_FrameSize);

		float Reward = MazeRewardFun(RLAgent);
		bool Terminal = MazeTerminal(RLAgent);

		TaskMsg = RLMazeStepMsgWrapper(RLAgent->GetName(), StrNextState, Reward, Terminal, RLAgent->GetTimeStamp());
		_CurrSteps++;
	}
	else TaskMsg = _TaskLastMsg;

	RLAgent->SetTaskMsg(TaskMsg);
	_TaskTimeStamp = RLAgent->GetTimeStamp();
	_TaskLastMsg = TaskMsg;
}

float UMazeRLTask::MazeRewardFun(ABaseAgent* RLAgent)
{
	FVector AgentLoc = RLAgent->GetActorLocation();

	float Reward = -0.02f;
	if (!_bSafeMovement)
		Reward -= 0.2;
	if ((AgentLoc - EndLoc).Size() < _ReachDistance)
		Reward += 100.0;

	return Reward;
}

bool UMazeRLTask::MazeTerminal(ABaseAgent* RLAgent)
{
	FVector AgentLoc = RLAgent->GetActorLocation();
	if ((AgentLoc - EndLoc).Size() < _ReachDistance || _CurrSteps >= _MaxSteps)
		return true;
	return false;
}

bool UMazeRLTask::RLMazeResetMsgParser(std::string Message, FString AgentName, uint64* TimeStamp, bool* Reply)
{
	RLMaze::ResetMsg AgentResetMsg;
	//if (!AgentResetMsg.ParseFromString(Message))
	if(!google::protobuf::TextFormat::ParseFromString(Message, &AgentResetMsg))
		return false;
	
	// Check field validation
	*Reply = AgentResetMsg.reply();
	if (*Reply) return false;

	// Get current message timestamp
	*TimeStamp = AgentResetMsg.time_stamp();

	return true;
}

bool UMazeRLTask::RLMazeStepMsgParser(std::string Message, FString AgentName, int ActionDim, float* AgentAction, uint64* TimeStamp, bool* Reply)
{
	RLMaze::StepMsg AgentStepMsg;
	//if (!AgentStepMsg.ParseFromString(Message))
	if(!google::protobuf::TextFormat::ParseFromString(Message, &AgentStepMsg))
		return false;

	// Check field validation
	*Reply = AgentStepMsg.reply();
	if (*Reply) return false;

	if (AgentStepMsg.action_size() != ActionDim) return false;
	for (int i = 0; i < AgentStepMsg.action_size(); i++)
	{
		const float Action = AgentStepMsg.action(i);
		AgentAction[i] = Action;
	}

	// Get current message timestamp
	*TimeStamp = AgentStepMsg.time_stamp();

	return true;
}

std::string UMazeRLTask::RLMazeResetMsgWrapper(FString AgentName, std::string State, uint64 TimeStamp)
{
	RLMaze::ResetMsg AgentResetMsg;
	std::string AgentResetStr;

	AgentResetMsg.set_state(State);
	AgentResetMsg.set_time_stamp(TimeStamp);
	AgentResetMsg.set_reply(true);

	if(google::protobuf::TextFormat::PrintToString(AgentResetMsg, &AgentResetStr))
		return AgentResetStr;
	return 0;
}

std::string UMazeRLTask::RLMazeStepMsgWrapper(FString AgentName, std::string NextState, float Reward, bool Terminal, uint64 TimeStamp)
{
	RLMaze::StepMsg AgentStepMsg;
	std::string AgentStepStr;

	AgentStepMsg.set_next_state(NextState);
	AgentStepMsg.set_reward(Reward);
	AgentStepMsg.set_terminal(Terminal);
	AgentStepMsg.set_time_stamp(TimeStamp);
	AgentStepMsg.set_reply(true);

	if (google::protobuf::TextFormat::PrintToString(AgentStepMsg, &AgentStepStr))
		return AgentStepStr;
	return 0;
}

uint8 UMazeRLTask::GetActionDim() const
{
	return _ActionDim;
}

uint8 UMazeRLTask::GetFrameSize() const
{
	return _FrameSize;
}

void UMazeRLTask::SetRenderTarget(UTextureRenderTarget2D* AgentRenderTarget)
{
	this->_TaskRenderTarget2D = AgentRenderTarget;
}