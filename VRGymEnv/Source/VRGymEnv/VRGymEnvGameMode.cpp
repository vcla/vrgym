// Fill out your copyright notice in the Description page of Project Settings.

#include "VRGymEnvGameMode.h"


AVRGymEnvGameMode::AVRGymEnvGameMode()
{

}

void AVRGymEnvGameMode::BeginPlay()
{
	Super::BeginPlay();
	PrimaryActorTick.bCanEverTick = true;

	TArray<AActor*> FoundComms;
	UGameplayStatics::GetAllActorsOfClass(GetWorld(), ACentralCommunicator::StaticClass(), FoundComms);
	for (auto Comm : FoundComms)
	{
		_GameAgentCommunicator = Cast<ACentralCommunicator>(Comm);
		if (_GameAgentCommunicator)
			break;
	}
}

void AVRGymEnvGameMode::Tick(float DeltaSeconds)
{
	Super::Tick(DeltaSeconds);

}

void AVRGymEnvGameMode::EndPlay(const EEndPlayReason::Type EndPlayReason)
{
	Super::EndPlay(EndPlayReason);


}

ACentralCommunicator* AVRGymEnvGameMode::GetCentralCommunicator() const
{
	return this->_GameAgentCommunicator;
}