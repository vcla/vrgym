// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "VRGymEnv.h"
#include "GameFramework/Actor.h"
#include "BaseObject.generated.h"

UCLASS()
class VRGYMENV_API ABaseObject : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ABaseObject();

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	// Called every frame
	virtual void Tick(float DeltaTime) override;

protected:
	// Set of inheritable attributes
	UMaterialInterface* ObjectMaterialInstance;
	bool MaterialOverridable;

	bool Movable;
	bool Splitable;

	// Set of inheritable attributes access
	bool IsMaterialOverridable();

	bool IsObjectMovable();
	bool IsObjectSplitable();

	virtual void Animation(float DeltaTime);
	
};
