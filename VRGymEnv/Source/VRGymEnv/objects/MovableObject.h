// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "VRGymEnv.h"
#include "BaseObject.h"
#include "GameFramework/Actor.h"
#include "MovableObject.generated.h"

UCLASS()
class VRGYMENV_API AMovableObject : public ABaseObject
{
	GENERATED_BODY()
	
public:	

	UPROPERTY(EditDefaultsOnly, Category = "MovableObject", Meta = (AllowPrivateAccess = "true"))
		class UStaticMeshComponent* MovableMesh;

	UPROPERTY(EditDefaultsOnly, Category = "MovableObject", Meta = (BlueprintProtected = "true"))
		FString ItemID;

	// Sets default values for this actor's properties
	AMovableObject();

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	// Called every frame
	virtual void Tick(float DeltaTime) override;

private:

	virtual void Animation(float DeltaTime) override;
	
};
