// Fill out your copyright notice in the Description page of Project Settings.

#include "PourContainerObject.h"


// Sets default values
APourContainerObject::APourContainerObject()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	UpArrow = CreateDefaultSubobject<UArrowComponent>(TEXT("ArrowRoot"));

	ContainerMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("ContainerMesh"));
	FillMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("FillMesh"));
	FluidParticles = CreateDefaultSubobject<UFluidPouringComponent>(TEXT("FluidParticles"));
	RootComponent = ContainerMesh;

	UpArrow->SetupAttachment(RootComponent);
	FluidParticles->SetupAttachment(UpArrow);
	FluidParticles->Deactivate();

	RemainingFluid = 0.05;
	FlowRate = 0.1;
}

// Called when the game starts or when spawned
void APourContainerObject::BeginPlay()
{
	Super::BeginPlay();
	
	FluidMaterialRef = FillMesh->CreateAndSetMaterialInstanceDynamic(0);
	FluidMaterialRef->SetVectorParameterValue(FName("FluidColor"), FluidColor);
	FluidParticles->SetColorParameter(FName("FluidColor"), FluidColor);
	FluidParticles->SetFloatParameter(FName("MouthRadius"), MouthRadius);
}

// Called every frame
void APourContainerObject::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	Animation(DeltaTime);
}

void APourContainerObject::Animation(float DeltaTime)
{
	MaxFillFraction = FVector::DotProduct(UpArrow->GetForwardVector(), FVector(0, 0, 1)) + RemainingFluid;

	if (MaxFillFraction < CurrentFillFraction)
	{
		if (CurrentFillFraction > 0)
		{
			FluidParticles->PourFluid(FluidColor, FlowRate * DeltaTime);
			CurrentFillFraction = CurrentFillFraction - (FlowRate * DeltaTime);
			FluidParticles->Activate();
		}
		else
		{
			ContainedFluids.Empty();
			FluidParticles->Deactivate();
		}
	}
	else
	{
		FluidParticles->Deactivate();
	}
	FluidMaterialRef->SetScalarParameterValue(FName("FillHeight"), CurrentFillFraction);
}

void APourContainerObject::RecieveFluid(FLinearColor NewFluidColor, float FluidAmount, FString FluidKind)
{
	FluidColor = (FluidColor * CurrentFillFraction + NewFluidColor * FluidAmount) / (CurrentFillFraction + FluidAmount);
	if (CurrentFillFraction < 1.0f)
	{
		CurrentFillFraction += FluidAmount;
	}
	FluidMaterialRef->SetVectorParameterValue(FName("FluidColor"), FluidColor);
	FluidParticles->SetColorParameter(FName("FluidColor"), FluidColor);
	ContainedFluids.AddUnique(FluidKind);
}