// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "VRGymEnv.h"
#include "Particles/ParticleSystemComponent.h"
#include "FluidPouringComponent.generated.h"

/**
 * 
 */
UCLASS()
class VRGYMENV_API UFluidPouringComponent : public UParticleSystemComponent
{
	GENERATED_BODY()
	
public:
	UFUNCTION(BlueprintCallable, Category = "Pouring")
		void PourFluid(FLinearColor FluidColor, float PourAmount);

	UPROPERTY(EditAnywhere, Category = "Pouring")
		FString FluidKind;
	
	
};
