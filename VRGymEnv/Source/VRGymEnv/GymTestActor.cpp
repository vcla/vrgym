// Fill out your copyright notice in the Description page of Project Settings.

#include "GymTestActor.h"

// Sets default values
AGymTestActor::AGymTestActor()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	PortEnv = -1;
	PortRecv = -1;
}

// Called when the game starts or when spawned
void AGymTestActor::BeginPlay()
{
	Super::BeginPlay();

	FParse::Value(FCommandLine::Get(), TEXT("port"), PortEnv);
	FParse::Value(FCommandLine::Get(), TEXT("port_ros_recv"), PortRecv);
	CommandLine = FCommandLine::Get();

	// game view port client rendering
	UGameViewportClient* gameViewport = GEngine->GameViewport;
	//gameViewport->bDisableWorldRendering = true;
	InViewport = gameViewport->Viewport;
}

// Called every frame
void AGymTestActor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	//GEngine->AddOnScreenDebugMessage(-1, 5.0f, FColor::Red, FString::Printf(*CommandLine));
	

}

