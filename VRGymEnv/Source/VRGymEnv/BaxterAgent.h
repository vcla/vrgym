// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "VRGymEnv.h"
#include "BaseAgent.h"
#include "tasks/MazeRLTask.h"
#include "tasks/MotionPlanTask.h"
#include "BaxterAgent.generated.h"

/**
 * 
 */

UENUM(BlueprintType)
enum class EBaxterAgentTask : uint8
{
	TASK_MAZERL,
	TASK_MOTIONPLAN
};

UCLASS()
class VRGYMENV_API ABaxterAgent : public ABaseAgent
{
	GENERATED_BODY()
	
public:
	ABaxterAgent();
	
	virtual void BeginPlay() override;

	virtual void Tick(float DeltaSeconds) override;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Baxter Assets")
		USceneComponent* _SceneBaxter;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Baxter Assets")
		UStaticMeshComponent* _Base;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Baxter Assets")
		UStaticMeshComponent* _Torso;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Baxter Assets")
		UStaticMeshComponent* _Head;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Baxter Assets")
		UStaticMeshComponent* _Screen;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Baxter Assets")
		UStaticMeshComponent* _LArmMount;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Baxter Assets")
		UStaticMeshComponent* _LUShoulder;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Baxter Assets")
		UStaticMeshComponent* _LLShoulder;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Baxter Assets")
		UStaticMeshComponent* _LUElbow;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Baxter Assets")
		UStaticMeshComponent* _LLElbow;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Baxter Assets")
		UStaticMeshComponent* _LUForearm;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Baxter Assets")
		UStaticMeshComponent* _LLForearm;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Baxter Assets")
		UStaticMeshComponent* _LWrist;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Baxter Assets")
		UStaticMeshComponent* _RArmMount;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Baxter Assets")
		UStaticMeshComponent* _RUShoulder;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Baxter Assets")
		UStaticMeshComponent* _RLShoulder;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Baxter Assets")
		UStaticMeshComponent* _RUElbow;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Baxter Assets")
		UStaticMeshComponent* _RLElbow;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Baxter Assets")
		UStaticMeshComponent* _RUForearm;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Baxter Assets")
		UStaticMeshComponent* _RLForearm;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Baxter Assets")
		UStaticMeshComponent* _RWrist;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Baxter Assets")
		USphereComponent* _BodySphere;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Baxter Assets")
		USceneCaptureComponent2D* _SceneCaptureComponent2D;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Baxter Assets")
		UTextureRenderTarget2D* _RenderTarget2D;

	// Available task resources
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Baxter Tasks")
		UMazeRLTask* _TaskMazeRL;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Baxter Tasks")
		UMotionPlanTask* _TaskMotionPlan;

private:
	EBaxterAgentTask _ETask;
	TMap<FString, UStaticMeshComponent*> _NameToMeshMap;
	virtual void InitializeTransMap(UStaticMeshComponent** C, const TCHAR* Name, const TCHAR* MeshPath) override;
	virtual void InitializeComponent() override;

	virtual void LaunchAnimation() override;
	virtual void ParseAgentData() override;
};
