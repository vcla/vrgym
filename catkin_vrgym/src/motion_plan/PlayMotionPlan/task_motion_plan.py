#! /usr/bin/env python

import rospy
import tf
from std_msgs.msg import String
from geometry_msgs.msg import Pose
from motion_plan.srv import *

import threading
import numpy as np
import time
import json
import ctypes

from proto_msgs import *
from google.protobuf import text_format

tf_frame_pairs = [
    ['/torso', '/head'],
    ['/torso', '/left_arm_mount'],
    ['/torso', '/right_arm_mount'],
    ['/left_arm_mount', '/left_upper_shoulder'],
    ['/right_arm_mount', '/right_upper_shoulder'],
    ['/left_upper_shoulder', '/left_lower_shoulder'],
    ['/right_upper_shoulder', '/right_lower_shoulder'],
    ['/left_lower_shoulder', '/left_upper_elbow'],
    ['/right_lower_shoulder', '/right_upper_elbow'],
    ['/left_upper_elbow', '/left_lower_elbow'],
    ['/right_upper_elbow', '/right_lower_elbow'],
    ['/left_lower_elbow', '/left_upper_forearm'],
    ['/right_lower_elbow', '/right_upper_forearm'],
    ['/left_upper_forearm', '/left_lower_forearm'],
    ['/right_upper_forearm', '/right_lower_forearm'],
    ['/left_lower_forearm', '/left_wrist'],
    ['/right_lower_forearm', '/right_wrist']
]


class MotionPlan:
    _taskID = 0

    def __init__(self):
        self._agent_name = 'BaxterAgent'
        self._agent_manage_pub = rospy.Publisher('/agent_manage', String, queue_size=1000)
        self._agent_manage_sub = rospy.Subscriber('/agent_res', String, callback=self.on_manage_sub, queue_size=1000)
        self._agent_data_pub = rospy.Publisher('/agent_data_req', String, queue_size=1000)
        self._agent_data_sub = rospy.Subscriber('/agent_data_res', String, callback=self.on_data_sub, queue_size=1000)
        
        while self._agent_manage_pub.get_num_connections() == 0:
            time.sleep(.001)
        
        while self._agent_data_pub.get_num_connections() == 0:
            time.sleep(.001)

        MotionPlan._taskID += 1
        self._taskID = MotionPlan._taskID
        self._agent_msg = None
        self._parsed_msg = None
        self._agent_res_recv = False
        self._in_manage = False
        self._in_tf_trans = False
        self._bind_env_host = None
        self._bind_env_port = None
        self._time_out = 30.0


    def agent_wait_execution(self):
        start_t = time.time()
        while not self._agent_res_recv:
            time.sleep(1e-9)
            end_t = time.time()
            if end_t - start_t > self._time_out:
                print('Agent Message Communication Failure')
                start_t = time.time()
                if not self._in_manage:
                    self._agent_data_pub.publish(json.dumps(self._agent_msg))
                else:
                    raise Exception('Agent Management Failed')


    def agent_tf_listen(self):
        listener = tf.TransformListener()

        while self._in_tf_trans:
            try:
                msg = JointTransformMsg()
                msg.agent_name = self._agent_name
                for frame_target, frame_source in tf_frame_pairs:
                    (trans, rot) = listener.lookupTransform(frame_target, frame_source, rospy.Time(0))

                    joint_msg = JointTransformMsg.JointMsg()
                    joint_msg.joint_name = frame_source[1:]
                    
                    joint_msg.pose.vector.x = trans[0]
                    joint_msg.pose.vector.y = trans[1]
                    joint_msg.pose.vector.z = trans[2]
                    
                    joint_msg.pose.quat.x = rot[0]
                    joint_msg.pose.quat.y = rot[1]
                    joint_msg.pose.quat.z = rot[2]
                    joint_msg.pose.quat.w = rot[3]

                    msg.joints_transform.extend([joint_msg])

            except (tf.LookupException, tf.ConnectivityException, tf.ExtrapolationException):
                continue

            msg_data = text_format.MessageToString(msg)
            self._agent_msg = {'agent_msg' : msg_data, \
                                'host' : self._bind_env_host, \
                                'port' : self._bind_env_port, \
                                'node_name' : rospy.get_name(), \
                                'task_id' : self._taskID, \
                                'agent_name' : self._agent_name}
            self._agent_data_pub.publish(json.dumps(self._agent_msg))
            self._agent_msg = None


    def agent_env_invoke(self):
        self._in_manage = True
        self._agent_msg = {'cmd' : 'ENVLAUNCH', \
                           'node_name' : rospy.get_name(), \
                           'task_id' : self._taskID}
        self._agent_manage_pub.publish(json.dumps(self._agent_msg))
        self.agent_wait_execution()
        self._agent_msg = None
        self._agent_res_recv = False
        self._in_manage = False

    
    def agent_env_close(self):
        self._in_manage = True
        self._agent_msg = {'cmd' : 'ENVCLOSE', \
                           'port' : self._bind_env_port, \
                           'node_name' : rospy.get_name(), \
                           'task_id' : self._taskID}
        self._agent_manage_pub.publish(json.dumps(self._agent_msg))
        self.agent_wait_execution()
        self._agent_msg = None
        self._agent_res_recv = False
        self._in_manage = False


    def agent_motion_exec(self, waypoints):
        tf_worker = threading.Thread(target=self.agent_tf_listen)
        tf_worker.daemon = True
        tf_worker.start()

        rospy.wait_for_service('huroco_right_arm/right_cartesian')
        motion_plan_srv = rospy.ServiceProxy('huroco_right_arm/right_cartesian', rightCartesian)

        self._in_tf_trans = True
        status_res = motion_plan_srv(waypoints)
        self._in_tf_trans = False

        tf_worker.join()


    def on_manage_sub(self, msg):
        obj = json.loads(msg.data)
        if not 'status' in obj.keys() or not 'node_name' in obj.keys() or not 'task_id' in obj.keys():
            raise Exception('message is wrongly received!')
        elif obj['node_name'] == rospy.get_name() and int(obj['task_id']) == self._taskID:
            if obj['status'] == 'failed':
                raise Exception('message status failed!')
            else:
                if obj['status'] == 'success':
                    self._bind_env_host = obj['host']
                    self._bind_env_port = int(obj['port'])
                    self._agent_res_recv = True
                elif obj['status'] == 'closed':
                    self._bind_env_host = None
                    self._bind_env_port = None
                    self._agent_res_recv = True
                else:
                    raise Exception('message parse wrongly!')
    
    
    def on_data_sub(self, msg):
        pass

    
if __name__ == '__main__':
    rospy.init_node('motion_plan_worker')

    pose0 = Pose()
    pose0.position.x = 1.0
    pose0.position.y = -0.5
    pose0.position.z = 0.5

    pose0.orientation.x = 0.0
    pose0.orientation.y = 0.707
    pose0.orientation.z = 0.0
    pose0.orientation.w = 0.707

    waypoints = [pose0]

    motion_plan = MotionPlan()
    motion_plan.agent_env_invoke()
    time.sleep(1.0)

    motion_plan.agent_motion_exec(waypoints)
    time.sleep(10.0)

    motion_plan.agent_env_close()