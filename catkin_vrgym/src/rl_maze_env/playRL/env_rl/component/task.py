import rospy
from std_msgs.msg import String

import numpy as np
import time
import json
import ctypes

import multiprocessing as mp
import sys

from proto_msgs import *
from google.protobuf import text_format


class MazeRL:
    _taskID = 0

    def __init__(self):
        self._agent_name = 'BaxterAgent'
        self._agent_manage_pub = rospy.Publisher('/agent_manage', String, queue_size=1000)
        self._agent_manage_sub = rospy.Subscriber('/agent_res', String, callback=self.on_manage_sub, queue_size=1000)
        self._agent_data_pub = rospy.Publisher('/agent_data_req', String, queue_size=1000)
        self._agent_data_sub = rospy.Subscriber('/agent_data_res', String, callback=self.on_data_sub, queue_size=1000)
    
        while self._agent_manage_pub.get_num_connections() == 0:
            time.sleep(.001)

        while self._agent_data_pub.get_num_connections() == 0:
            time.sleep(.001)

        MazeRL._taskID += 1
        self._taskID = MazeRL._taskID
        self._agent_msg = None
        self._parsed_msg = None
        self._agent_res_recv = False
        self._in_manage = False
        self._in_reset = False
        self._in_step = False
        self._bind_env_host = None
        self._bind_env_port = None
        self._last_data_time_stamp = 0
        self._time_out = 30.0

        self._state_dim = (3, 84, 84)
        self._action_dim = 3

    
    # time out added in case of package loss during ROS communication
    def agent_wait_execution(self):
        start_t = time.time()
        while not self._agent_res_recv:
            time.sleep(1e-9)
            end_t = time.time()
            if end_t - start_t > self._time_out:
                print('Agent Message Communication Failure')
                start_t = time.time()
                if not self._in_manage:
                    self._agent_data_pub.publish(json.dumps(self._agent_msg))
                else:
                    raise Exception('Agent Management Failed')

    
    # agent invoke new vrgym env
    def agent_env_invoke(self):
        self._in_manage = True
        self._agent_msg = {'cmd' : 'ENVLAUNCH', \
                           'node_name' : rospy.get_name(), \
                           'task_id' : self._taskID}
        self._agent_manage_pub.publish(json.dumps(self._agent_msg))
        self.agent_wait_execution()
        self._agent_msg = None
        self._agent_res_recv = False
        self._in_manage = False


    # agent close vrgym env
    def agent_env_close(self):
        self._in_manage = True
        self._agent_msg = {'cmd' : 'ENVCLOSE', \
                           'port' : self._bind_env_port, \
                           'node_name' : rospy.get_name(), \
                           'task_id' : self._taskID}
        self._agent_manage_pub.publish(json.dumps(self._agent_msg))
        self.agent_wait_execution()
        self._agent_msg = None
        self._agent_res_recv = False
        self._in_manage = False


    # agent reset vrgym env
    def agent_env_reset(self):
        self._in_reset = True
        msg = RLMazeResetMsg()
        msg.time_stamp = ctypes.c_uint64(int(time.time()*1e7)).value
        msg.reply = False
        msg_data = text_format.MessageToString(msg)

        self._agent_msg = {'agent_msg' : msg_data, \
                           'host' : self._bind_env_host, \
                           'port' : self._bind_env_port, \
                           'node_name' : rospy.get_name(), \
                           'task_id' : self._taskID, \
                           'agent_name' : self._agent_name}
        self._agent_data_pub.publish(json.dumps(self._agent_msg))
        self.agent_wait_execution()
        self._agent_msg = None
        self._agent_res_recv = False
        self._in_reset = False

        state = np.frombuffer(self._parsed_msg.state, dtype=np.uint8)
        return state


    # agent step vrgym env
    def agent_env_step(self, action):
        self._in_step = True
        msg = RLMazeStepMsg()
        # msg.action.extend([0.0, 0.0, 0.0])
        msg.action.extend(action.tolist())
        msg.time_stamp = ctypes.c_uint64(int(time.time()*1e7)).value
        msg.reply = False
        msg_data = text_format.MessageToString(msg)

        self._agent_msg = {'agent_msg' : msg_data, \
                           'host' : self._bind_env_host, \
                           'port' : self._bind_env_port, \
                           'node_name' : rospy.get_name(), \
                           'task_id' : self._taskID, \
                           'agent_name' : self._agent_name}
        self._agent_data_pub.publish(json.dumps(self._agent_msg))
        self.agent_wait_execution()
        self._agent_msg = None
        self._agent_res_recv = False
        self._in_step = False

        next_state = np.frombuffer(self._parsed_msg.next_state, dtype=np.uint8)
        reward = float(self._parsed_msg.reward)
        terminal = int(self._parsed_msg.terminal)
        if terminal:
            state = self.agent_env_reset()
            return state, reward, terminal
        else:
            return next_state, reward, terminal


    def on_manage_sub(self, msg):
        obj = json.loads(msg.data)
        if not 'status' in obj.keys() or not 'node_name' in obj.keys() or not 'task_id' in obj.keys():
            raise Exception('message is wrongly received!')
        elif obj['node_name'] == rospy.get_name() and int(obj['task_id']) == self._taskID:
            if obj['status'] == 'failed':
                raise Exception('message status failed!')
            else:
                if obj['status'] == 'success':
                    self._bind_env_host = obj['host']
                    self._bind_env_port = int(obj['port'])
                    self._agent_res_recv = True
                elif obj['status'] == 'closed':
                    self._bind_env_host = None
                    self._bind_env_port = None
                    self._agent_res_recv = True
                else:
                    raise Exception('message parse wrongly!')


    def on_data_sub(self, msg):
        obj = json.loads(msg.data)
        if not 'agent_msg' in obj.keys() or not 'node_name' in obj.keys() or not 'task_id' in obj.keys() or \
        not 'host' in obj.keys() or not 'port' in obj.keys():
            raise Exception('message is wrongly received!')
        elif obj['node_name'] == rospy.get_name() and int(obj['task_id']) == self._taskID:
            if obj['host'] == self._bind_env_host and int(obj['port']) == self._bind_env_port:
                # print(obj['node_name'] + ' ' + str(obj['task_id']) + ' agent received: ' + obj['agent_msg'])
                if self._in_reset:
                    parsed_msg = RLMazeResetMsg()
                    text_format.Merge(obj['agent_msg'], parsed_msg)
                    if self._last_data_time_stamp == parsed_msg.time_stamp: return
                    self._last_data_time_stamp = parsed_msg.time_stamp
                    self._parsed_msg = parsed_msg
                elif self._in_step:
                    parsed_msg = RLMazeStepMsg()
                    text_format.Merge(obj['agent_msg'], parsed_msg)
                    if self._last_data_time_stamp == parsed_msg.time_stamp: return
                    self._last_data_time_stamp = parsed_msg.time_stamp
                    self._parsed_msg = parsed_msg
                else:
                    raise Exception('message is wrongly parsed!')
                self._agent_res_recv = True


class ProcessAgentTask:
    _nodeID = 0
    def __init__(self, Task):
        ProcessAgentTask._nodeID += 1
        self._nodeID = ProcessAgentTask._nodeID
        self._pipe, worker_pipe = mp.Pipe()
        self._worker = ProcessWrapper(worker_pipe, Task, self._nodeID)
        self._worker.start()
        time.sleep(1.0)
        # task params
        self._pipe.send([ProcessWrapper.SPECS, None])
        self.state_dim, self.action_dim = self._pipe.recv()


    def invoke(self):
        self._pipe.send([ProcessWrapper.INVOKE, None])
        self._invoked = self._pipe.recv()
        print('agent env invoked')
    
    def reset(self):
        self._pipe.send([ProcessWrapper.RESET, None])
        return self._pipe.recv()

    def step(self, action):
        self._pipe.send([ProcessWrapper.STEP, action])
        return self._pipe.recv()
    
    def close(self):
        self._pipe.send([ProcessWrapper.CLOSE, None])
        self._pipe.recv()
        print('agent env closed')


class ProcessWrapper(mp.Process):
    INVOKE = 0
    RESET = 1
    STEP = 2
    CLOSE = 3
    SPECS = 4

    def __init__(self, pipe, Task, node_id):
        mp.Process.__init__(self)
        self._pipe = pipe
        self._Task = Task
        self._node_id = node_id

    def run(self):
        rospy.init_node('agent_worker'+str(self._node_id))
        task = self._Task()
        while True:
            op, data = self._pipe.recv()
            if op == self.INVOKE:
                self._pipe.send(task.agent_env_invoke())
            elif op == self.RESET:
                self._pipe.send(task.agent_env_reset())
            elif op == self.STEP:
                self._pipe.send(task.agent_env_step(data))
            elif op == self.CLOSE:
                self._pipe.send(task.agent_env_close())
                self._pipe.close()
                return
            elif op == self.SPECS:
                self._pipe.send([task._state_dim, task._action_dim])
            else:
                raise Exception('Unknown Env Command')


class ParallelizedTask:
    def __init__(self, Task, num_workers):
        self.tasks = [ProcessAgentTask(Task) for _ in range(num_workers)]
        self.state_dim = self.tasks[0].state_dim
        self.action_dim = self.tasks[0].action_dim
    
    def invoke(self):
        for task in self.tasks: task.invoke()
        time.sleep(1.0)

    def close(self):
        for task in self.tasks: task.close()

    def reset(self):
        results = [task.reset() for task in self.tasks]
        return np.stack(results)

    def step(self, actions):
        results = [task.step(action) for task, action in zip(self.tasks, actions)]
        results = map(lambda x: np.stack(x), zip(*results))
        return results