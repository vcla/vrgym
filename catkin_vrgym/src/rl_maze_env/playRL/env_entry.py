#! /usr/bin/env python

from env_rl import *


def rl_maze_ppo():
    config = Config()
    config.num_workers = 8

    Task = MazeRL
    config.task_fn = lambda: ParallelizedTask(Task, config.num_workers)
    config.network_fn = lambda state_dim, action_dim: GaussianActorCriticNet(
        state_dim, action_dim, phi_body=NatureConvBody(in_channels=1),
        gpu=0)
    config.optimizer_fn = lambda params: torch.optim.RMSprop(params, lr=0.00025)
    config.state_trans_fn = lambda states, state_dim: state_trans_rgb2gray(states, state_dim)

    config.discount = 0.98
    config.use_gae = True
    config.gae_tau = 0.95
    config.gradient_clip = 0.5
    config.rollout_length = 50
    config.optimization_epochs = 10
    config.num_mini_batches = 16
    config.ppo_ratio_clip = 0.2
    run_iterations(PPOAgent(config))


if __name__ == '__main__':
    rl_maze_ppo()
    rospy.spin()